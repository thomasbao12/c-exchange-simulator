#include <algorithm>
#include <list>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include "quickcheck/quickcheck.hh"

using namespace std;

typedef unsigned long oid_t;

struct PriceLevel 
{
	PriceLevel() {}

	//copying of mOrdersById is not trivial
	PriceLevel(const PriceLevel& that) :
		mOrders(that.mOrders),
		mOIDs(that.mOIDs)
	{
		for(auto it = mOrders.cbegin(); it != mOrders.end(); ++it)
			this->mOrdersById[it->oid] = it;
	}
	PriceLevel& operator=(const PriceLevel& that) = delete;

	struct InternalOrder
	{
		oid_t oid;
		unsigned int qty;
		char side;
	};
	
	struct InternalMatch
	{
		InternalMatch(oid_t id, unsigned int qty) :
		oid(id),
		fillQty(qty)
		{}

		oid_t oid;
		unsigned int fillQty;
	};
		
	void cancelOrder(oid_t oid) {
		auto it = mOrdersById.find(oid);
		if (it == mOrdersById.end())	
			return;
			//return "E order " + to_string(oid) + " does not exist";
		mOrders.erase(it->second);
		mOrdersById.erase(it);
		return;
		//return "X " + to_string(oid);
	}

	list<InternalMatch> placeOrder(InternalOrder order)
	{
		list<InternalMatch> matches;
		if (order.qty == 0 || mOIDs.find(order.oid) != mOIDs.end())
			return matches;
		mOIDs.insert(order.oid);

		if (mOrders.empty() || mOrders.front().side == order.side)
		{
			mOrders.push_back(order);
			mOrdersById[order.oid] = --mOrders.cend(); 
			return matches;
		}	
		
		unsigned int totalMatched = 0;
		while (order.qty != 0 && !mOrders.empty()) 
		{
			unsigned int matched = min(order.qty, mOrders.front().qty);	
			order.qty -= matched;
			mOrders.front().qty -= matched;
			matches.emplace_back(mOrders.front().oid, matched);
			if (mOrders.front().qty == 0)
				mOrders.pop_front();
			totalMatched += matched;
		}
		if (order.qty != 0)
			mOrders.push_back(order);
		matches.emplace_back(order.oid, totalMatched);
		//mOrdersById[order.oid] = --mOrders.cend(); 
		return matches;
	}	
	
	bool empty() const { return mOrders.empty(); }
	InternalOrder& front() { return mOrders.front(); }
	InternalOrder& back() { return mOrders.back(); }

	double mPx;
	list<InternalOrder> mOrders;
	unordered_map<oid_t, list<InternalOrder>::const_iterator> mOrdersById;
	unordered_set<oid_t> mOIDs;
};

/*
int main() {
	PriceLevel level;
	PriceLevel::InternalOrder o1{1000, 1, 'B'};
	PriceLevel::InternalOrder o2{1001, 2, 'B'};
	level.cancelOrder(1000);
	level.placeOrder(o1);
	level.placeOrder(o2);
	//cout << level.cancelOrder(1000) << endl;
	//cout << level.cancelOrder(1001) << endl;
	PriceLevel::InternalOrder o3{1002, 1, 'S'};
	PriceLevel::InternalOrder o4{1003, 1, 'B'};
	level.placeOrder(o3);
	auto result = level.placeOrder(o4);
	cout << result.front().oid << " " << result.front().fillQty << endl;
	cout << result.back().oid << " " << result.back().fillQty << endl;
}*/
